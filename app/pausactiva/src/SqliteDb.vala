using GLib;
using Gee;

namespace Bsx {

public class SqliteDb  : GLib.Object {

	private Sqlite.Database db = null;
	private int ec = -1;

	public SqliteDb(){
		this.ec = Sqlite.Database.open_v2 ("pausactiva.db", out db, Sqlite.OPEN_READONLY);
		if (this.ec != Sqlite.OK) {
			stderr.printf ("Can't open database: %d: %s\n", db.errcode (), db.errmsg ());
		}
	}

	public unowned Sqlite.Database getConnection(){
		return this.db;
	}

	public Gee.List<HashMap<string,string>> query( string query )
	{
		
		Gee.List<HashMap<string,string>> res = new ArrayList<HashMap<string,string>>();
		string errmsg;

		this.ec = this.db.exec (query,  (n_columns, values, column_names) => { 
			HashMap<string,string> item = null;
			for (int i = 0; i < n_columns; i++) {
				stdout.printf ("%s = %s\n", column_names[i], values[i]);
				item = new HashMap<string,string>();
				item.set( column_names[i] , values[i]);
				res.add(item);
			}
			return 0;
		}, out errmsg);

		if (this.ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			res = null;
		}

		return res;
	}

}

}