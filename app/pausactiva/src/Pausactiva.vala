// Basic GTK+ application template to save myself the work of having to do this over and over.
// Replace this comment with an app description or something.

// TODO: #1 Make an XML version.

using Gtk;
using Granite;
namespace Bsx {
public class Pausactiva : Gtk.Application {
Gtk.ProgressBar progress_bar;

public Pausactiva () {
	// Define the application ID and flags.
	// REMEMBER: CHANGE APP ID TO MATCH PROJECT NAME
	Object (
		application_id: "com.besixplus.pausactiva",
		flags : ApplicationFlags.FLAGS_NONE
		);
}

protected override void activate () {

	var css_provider = new Gtk.CssProvider();
	css_provider.load_from_resource("/com/besixplus/pausactiva/stylesheet.css");

	var app_settings = Bsx.Services.ConfigManager.get_default ();
	var gtk_settings = Gtk.Settings.get_default();

	var dark = app_settings.get_boolean("dark");
	gtk_settings.gtk_application_prefer_dark_theme = dark;

	var main_window = new Gtk.ApplicationWindow (this);
	var screen = main_window.get_screen ();
	main_window.set_default_size (800, 600);
	main_window.window_position = WindowPosition.CENTER;
	main_window.title = _("App title");
	main_window.set_border_width(10);
	try{
		main_window.icon = new Gdk.Pixbuf.from_resource_at_scale ("/com/besixplus/pausactiva/pausactiva.svg",48,48,true);
	} catch (Error e) {
		stderr.printf ("Could not load application icon: %s\n", e.message);
	}

	Gtk.StyleContext.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

	Gtk.HeaderBar headerbar = new Gtk.HeaderBar();
	headerbar.show_close_button = true;
	headerbar.title = _("App title");
	main_window.set_titlebar(headerbar);

	Gtk.Menu menu = new Gtk.Menu ();
	var pref_item = new Gtk.MenuItem.with_label (_("Preferences"));
	var about_item = new Gtk.MenuItem.with_label (_("About"));
	menu.add (pref_item);
	menu.add (about_item);
	pref_item.activate.connect (() => {
				var preferences = new Bsx.Widgets.Settings(main_window);
				preferences.run ();
			});
	about_item.activate.connect (() => {
				var about = new Bsx.Widgets.About ();
				about.show ();
			});

	var app_button = new Gtk.MenuButton ();
	app_button.popup = menu;
	app_button.tooltip_text = _("Options");
	app_button.image = new Gtk.Image.from_icon_name ("open-menu-symbolic", Gtk.IconSize.BUTTON);
	menu.show_all ();

	headerbar.pack_end(app_button);

	var image = new Gtk.Image ();
	image.set_from_file ("pausactiva.svg");

	Gtk.Box box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);

	var lblCat = new Gtk.Label (_("Cat title"));
	lblCat.get_style_context().add_class("class1");

	progress_bar = new Gtk.ProgressBar ();

	box.pack_start (lblCat, false, false, 0);
	box.pack_start (new Gtk.Label (_("title")), false, false, 0);
	box.pack_start (image, true, true, 0);
	box.pack_start (progress_bar, false, false, 0);
	main_window.add (box);

	main_window.show_all();

	double fraction = 0.0;
	progress_bar.set_fraction (fraction);
	GLib.Timeout.add (500, fill);

}

bool fill () {
	double fraction = progress_bar.get_fraction (); //get current progress
	fraction += 0.1; //increase by 10% each time this function is called

	progress_bar.set_fraction (fraction);

	/* This function is only called by GLib.Timeout.add while it returns true; */
	if (fraction < 1.0)
		return true;
	return false;
}

public static int main (string[] args) {
	var app = new Pausactiva ();
	return app.run (args);
}
}
}