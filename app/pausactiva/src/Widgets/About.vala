/*
* Copyright (c) 2018 Dirli <litandrej85@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*/
using Gdk;

namespace Bsx.Widgets {
    public class About : Gtk.AboutDialog {
        public About () {

            try{
                var logox = new Pixbuf.from_file("play_pause_icon.png");
                logo = logox;
            } catch (GLib.Error e) {
                stdout.printf("Error: %s\n", e.message);
           }

            modal = true;
            destroy_with_parent = true;
            authors = {"Klever Lascano <kleverlascano@gmail.com>"};
            comments = _("An app to do exercise while you make your job");
            license_type = Gtk.License.GPL_2_0;
            program_name = "Pausactiva";
            translator_credits = "--";
            website = "https://www.besixplus.com";
            website_label = _("BESIXPLUS CIA. LTDA.");
            response.connect (() => {destroy ();});
            show_all ();
        }
    }
}
