/*
* Copyright (c) 2018 Dirli <litandrej85@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*/
using Gtk;
using Gdk;

namespace Bsx.Widgets {

    public class Settings : Gtk.Dialog {
        public Settings (Gtk.Window window) {

            string? flag = null;

            var gtk_settings = Gtk.Settings.get_default();
            var app_settings = Bsx.Services.ConfigManager.get_default ();

            resizable = false;
            deletable = false;
            transient_for = window;
            modal = true;
            title = _("Preferences");

            Gtk.Label tit1_pref = new Gtk.Label (_("Interface"));
            tit1_pref.get_style_context ().add_class ("preferences");
            tit1_pref.halign = Gtk.Align.START;
            var tit2_pref = new Gtk.Label (_("General"));
            tit2_pref.get_style_context ().add_class ("preferences");
            tit2_pref.halign = Gtk.Align.START;

            Gtk.Label icon_label = new Gtk.Label (_("Dark mode") + ":");
            icon_label.halign = Gtk.Align.END;
            Gtk.Switch icon = new Gtk.Switch ();
            icon.halign = Gtk.Align.START;
            //icon.bind_property ("active", gtk_settings, "gtk_application_prefer_dark_theme");  
            //icon.active = dark;

            Gtk.Label ind_label = new Gtk.Label (_("System tray indicator") + ":");
            ind_label.halign = Gtk.Align.END;
            Gtk.Switch ind = new Gtk.Switch ();
            ind.halign = Gtk.Align.START;

            Gtk.Label loc_label = new Gtk.Label (_("Autostart") + ":");
            loc_label.halign = Gtk.Align.END;
            Gtk.Switch loc = new Gtk.Switch ();
            loc.halign = Gtk.Align.START;

            Gtk.Label update_lab = new Gtk.Label (_("show every") + " :");
            update_lab.halign = Gtk.Align.END;
            Gtk.SpinButton update_box = new Gtk.SpinButton.with_range (10, 120, 1);
            update_box.set_halign (Gtk.Align.END);
            update_box.set_width_chars (4);

            var layout = new Gtk.Grid ();
            layout.valign = Gtk.Align.START;
            layout.column_spacing = 12;
            layout.row_spacing = 12;
            layout.margin = 12;
            layout.margin_top = 0;

            layout.attach (tit1_pref,  0, 0, 2, 1);

            layout.attach (icon_label, 0, 1, 1, 1);
            layout.attach (icon,       1, 1, 1, 1);

            layout.attach (tit2_pref,  0, 2, 2, 1);

            layout.attach (ind_label,  0, 3, 1, 1);
            layout.attach (ind,        1, 3, 1, 1);

            layout.attach (loc_label, 0, 4, 1, 1);
            layout.attach (loc      , 1, 4, 2, 1);

            layout.attach (update_lab, 0, 5, 1, 1);
            layout.attach (update_box, 1, 5, 2, 1);

            Gtk.Box content = this.get_content_area () as Gtk.Box;
            content.valign = Gtk.Align.START;
            content.border_width = 6;
            content.add (layout);

            add_button (_("Close"), Gtk.ResponseType.CLOSE);
            this.response.connect ((source, response_id) => {

                switch (response_id) {
                    case Gtk.ResponseType.CLOSE:
                        destroy ();
                    break;
                }
            });

            show_all ();
            
            app_settings.bind("auto", loc, "active", GLib.SettingsBindFlags.DEFAULT);
            app_settings.bind("indicator", ind, "active", GLib.SettingsBindFlags.DEFAULT);
            app_settings.bind("interval", update_box, "value", SettingsBindFlags.DEFAULT);
            app_settings.bind("dark", icon, "active", SettingsBindFlags.DEFAULT);

            loc.state_set.connect((state) => {
                flag = app_settings.get_boolean ("auto") ? "" : "0";
                return false;
            });

            ind.state_set.connect((state) => {
                flag = app_settings.get_boolean ("indicator") ? "" : "0";
                return false;
            });

            icon.state_set.connect((state) => {
                gtk_settings.gtk_application_prefer_dark_theme = state;
                 return false;
            });

        }
    }
}
